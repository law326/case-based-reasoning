/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cbr;

import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap; 
import java.util.List; 
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author henry
 */
public class MySQLDatabase {

    Connection conn = null;
    String url = "jdbc:mysql://localhost:3306/";
    String driver = "com.mysql.jdbc.Driver";
    String userName = "root";
    String password = "test";
    Statement stmt;
    String status;
    
    
    public void CreateDatabase(String DBName, String tableName, Map<String, Integer> CasesMap) {
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            System.out.println("Connected to the " + DBName + " database");
            
            int caseID = 0;
            try {
                stmt = conn.createStatement();
                String query = "SELECT id FROM caselist WHERE case_name ='" + tableName + "'";
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    caseID = rs.getInt(1);
                }
                System.out.println("Caselist " + tableName + " found");
                
                try {
                    stmt = conn.createStatement();
                    String statement = "CREATE TABLE `" + caseID + "`(frequency int, topterms varchar(500))";
                    stmt.executeUpdate(statement);
                    System.out.println("Table " + tableName + " created");
                } catch (SQLException s) {
                    System.out.println(s);
                }
                for (String term : CasesMap.keySet()) {
                    stmt = conn.createStatement();
                    String statement = "INSERT INTO `" + caseID + "`(topterms,frequency) VALUES('" + term + "',"
                            + CasesMap.get(term) + ")";
                    stmt.executeUpdate(statement);
                }
                
            } catch (SQLException s) {
                System.out.println(s);
            }
            
            conn.close();
            System.out.println("Disconnected from database");
            System.out.println();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.out.println("Could not connect to DB");
            System.out.println(e.getMessage());
            System.out.println();
        }
    }

    public void insertDomain(String DBName, String tableName, List<String> keywordList, List<String> conclusionList) {
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            System.out.println("Connected to the " + DBName + " database");
            try {
                stmt = conn.createStatement();
                String statement = "CREATE TABLE `" + tableName + "`(keyword varchar(100))";
                stmt.executeUpdate(statement);
                System.out.println("Table " + tableName + " created");
            } catch (SQLException s) {
                System.out.println(s);
            }
            for(int i = 0; i < keywordList.size(); i++){
                if(!(keywordList.get(i)).isEmpty()){
                    stmt = conn.createStatement();
                    String statement = "INSERT INTO `" + tableName + "`(keyword) VALUES('" + keywordList.get(i) + "')";
                    stmt.executeUpdate(statement);
                }
            }

            try {
                stmt = conn.createStatement();
                String statement = "CREATE TABLE `" + tableName + "_conclusion`(conclusion_keyword varchar(100))";
                stmt.executeUpdate(statement);
                System.out.println("Table " + tableName + "_conclusion created");
            } catch (SQLException s) {
                System.out.println(s);
            }
            for (int i = 0; i < conclusionList.size(); i++) {
                if(!(conclusionList.get(i)).isEmpty()){
                    stmt = conn.createStatement();
                    String statement = "INSERT INTO `" + tableName + "_conclusion`(conclusion_keyword) VALUES('" + conclusionList.get(i) + "')";
                    stmt.executeUpdate(statement);
                }
            }
            
            conn.close();
            System.out.println("Disconnected from database");
            System.out.println();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.out.println("Could not connect to DB");
            System.out.println(e.getMessage());
            System.out.println();
        }
    }
    
    
    public void CreateDatabase(String DBName, List<String> domainFiles) {
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            System.out.println("Connected to the " + DBName + " database");
            try {
                stmt = conn.createStatement();
                stmt.executeUpdate("CREATE TABLE domainList(domain_name varchar(500))");
                System.out.println("Table domainList created");
                for (int i = 0; i < domainFiles.size(); i++) {
                    stmt.executeUpdate("INSERT INTO domainList(domain_name) VALUES('" + domainFiles.get(i)  + "')");
                }
            } catch (SQLException s) {
                System.out.println(s);
            }
            conn.close();
            System.out.println("Disconnected from database");
            System.out.println();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.out.println("Could not connect to DB");
            System.out.println(e.getMessage());
            System.out.println();
        }
    }
    
    public void CreateCaseList(String DBName, List<String> caselist) {
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            System.out.println("Connected to the " + DBName + " database");
            try {
                stmt = conn.createStatement();
                stmt.executeUpdate("CREATE TABLE caseList(id int NOT NULL AUTO_INCREMENT, case_name varchar(500), PRIMARY KEY (id))");
                System.out.println("Table caseList created");
                for (int i = 0; i < caselist.size(); i++) {
                    stmt.executeUpdate("INSERT INTO caseList(case_name) VALUES('" + caselist.get(i) + "')");
                }
            } catch (SQLException s) {
                System.out.println(s);
            }
            conn.close();
            System.out.println("Disconnected from database");
            System.out.println();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.out.println("Could not connect to DB");
            System.out.println(e.getMessage());
            System.out.println();
        }
    }
    
    public void CreateCasebase(String DBName, List<String> modelCasesNames, List<String> classifiedDomains) {
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            System.out.println("Connected to the " + DBName + " database");
            try {
                stmt = conn.createStatement();
                stmt.executeUpdate("CREATE TABLE casebase(modelcase_name varchar(500), classified_domain varchar(500))");
                System.out.println("Table casebase created");
                for (int i = 0; i < modelCasesNames.size(); i++) {
                    String ModelCaseName = modelCasesNames.get(i);
                    String ClassifiedDomain = classifiedDomains.get(i);
                    ModelCaseName = ModelCaseName.replaceAll("'", "''");
                    ClassifiedDomain = ClassifiedDomain.replaceAll("'", "''");
                    String statement = "INSERT INTO casebase(modelcase_name,classified_domain) VALUES('" + ModelCaseName
                            + "','" + ClassifiedDomain + "')";
                    stmt.executeUpdate(statement);
                }
            } catch (SQLException s) {
                System.out.println(s);
            }
            conn.close();
            System.out.println("Disconnected from database");
            System.out.println();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.out.println("Could not connect to DB");
            System.out.println(e.getMessage());
            System.out.println();
        }
    }
    
    public void saveCaseFile(String DBName, String fileName, String filePath) throws FileNotFoundException{
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            System.out.println("Connected to the " + DBName + " database");
            
            try {
                stmt = conn.createStatement();
                String statement = "CREATE TABLE file(id int NOT NULL AUTO_INCREMENT, file VARCHAR(300), file_path VARCHAR(500), PRIMARY KEY (id))";
                stmt.executeUpdate(statement);
                System.out.println("Table file created");
                
                stmt = conn.createStatement();
                statement = "INSERT INTO file(file, file_path) values ('"+ fileName +"','"+ filePath +"')";
                stmt.executeUpdate(statement);
                System.out.println("File saved");
            } catch (SQLException s) {
                System.out.println("Table already exist. Inserting file.");
                if(((SQLException) s).getErrorCode()==1050){
                    stmt = conn.createStatement();
                    String statement = "INSERT INTO file(file, file_path) values ('" + fileName + "','" + filePath + "')";
                    stmt.executeUpdate(statement);
                    System.out.println("File saved");
                }
                
            }
            conn.close();
            System.out.println("Disconnected from database");
            System.out.println();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.out.println("Could not connect to DB");
            System.out.println(e.getMessage());
            System.out.println();
        }
    }

    public Map<String, Integer> RetrieveCase(String DBName, String tableName) {
        Map<String, Integer> map = new HashMap<>();
        List<Integer> freq = new ArrayList<>();
        List<String> topTerms = new ArrayList<>();
        int caseID = 0;
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            String query = "SELECT id FROM caseList WHERE case_name ='" + tableName + "'";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                caseID = rs.getInt(1);
            } 
            
            query = "SELECT * FROM `" + caseID + "`";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                freq.add(rs.getInt(1));
                topTerms.add(rs.getString(2));
            } 
            for (int k = 0; k < topTerms.size(); k++) {
                map.put(topTerms.get(k), freq.get(k));
            }
            conn.close();
        }
        catch(ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e){ }
        return map; 
    }

    
    public List<String> RetrieveNames(String DBName, String typeOfEntity) {
        Map<String, String> EntityNameList = new HashMap<>();
        List<String> maps = new ArrayList<>();
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            String tableName = typeOfEntity + "list";
            String query = "SELECT * FROM " + tableName;
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                //EntityNameList.put(rs.getString(1), rs.getString(2));
                maps.add(rs.getString(1));
            }
            conn.close();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException s) {
            System.out.println(s);
        }
        return maps;
    }
    
    public Map<String, String> RetrieveCaseBase(String DBName) {
        List<String> modelCases = new ArrayList<>();
        List<String> classifiedDomains = new ArrayList<>();
        Map<String, String> casebase = new HashMap<>();
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            String query = "SELECT * FROM casebase";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                modelCases.add(rs.getString(1));
                classifiedDomains.add(rs.getString(2));
            }
            for (int i = 0; i < modelCases.size(); i++) {
                casebase.put(modelCases.get(i), classifiedDomains.get(i));
            }
            conn.close();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException s) {
            System.out.println(s);
        }
        return casebase;
    }
    
    
    public List<String> RetrieveDomainModelCases(String DBName, String classifiedDomain) {
        List<String> relevantDomainModelCases = new ArrayList<>();
        String query;
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            
            if("all".equals(classifiedDomain))
                query = "SELECT modelcase_name FROM casebase";
            else
                query = "SELECT modelcase_name FROM casebase WHERE classified_domain='" + classifiedDomain + "'";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                relevantDomainModelCases.add(rs.getString(1));
            }
            conn.close();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException s) {
            System.out.println(s);
        }
        return relevantDomainModelCases;
    }
    
    public String getFilePath(String DBName, String fileName){
        String filePath = null;
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            String query = "SELECT file_path FROM file WHERE file ='" + fileName + "'";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                filePath = rs.getString(1);
            }
            conn.close();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException s) {
            System.out.println(s);
        }
        return filePath;
    }
    
    public List<String> getKeywordList(String DBName, String fileName) {
        List<String> keywordList = new ArrayList<>();
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            String query = "SELECT * FROM `" + fileName + "`";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                keywordList.add(rs.getString(1));
            }
            conn.close();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException s) {
            System.out.println(s);
        }
        return keywordList;
    }
    
    public Boolean isDatabaseExist(String DBName){
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + DBName, userName, password);
            stmt = conn.createStatement();
            stmt.getConnection();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException s) {
            System.out.println(s);
            return false;
        }
        return true;
    }
    
    public Boolean isDatabaseCreated(String DBName) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Class.forName(driver).newInstance();
        conn = DriverManager.getConnection(url, userName, password);
        System.out.println("Connected to the MySQL DB");
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate("CREATE DATABASE `" + DBName + "`");
            System.out.println("Database " + DBName + " created");
        } catch (SQLException s) {
            if (((SQLException) s).getErrorCode() == 1007) {
                int n = JOptionPane.showConfirmDialog(null, "Database " + DBName + " is already exist. \n" + "Do you want to overwrite it?", "Database Error", JOptionPane.YES_NO_OPTION);
                if (n == 0) {
                    stmt = conn.createStatement();
                    stmt.executeUpdate("DROP DATABASE `" + DBName + "`");
                    System.out.println("Database " + DBName + " deleted");
                    stmt.executeUpdate("CREATE DATABASE `" + DBName + "`");
                    System.out.println("Database " + DBName + " created");
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}
