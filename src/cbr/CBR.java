/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cbr;

import Classes.Domain;
import Classes.Cases;
import cbr.KStemmer.KStemmer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.TermFreqVector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;


/**
 *
 * @author henry
 */
public class CBR{

    List<Map> domainMaps = new ArrayList<>();
    List<Map> modelCasesMaps = new ArrayList<>();
    List<String> relevantDomainModelCases = new ArrayList<>(); 
    Map<String, Integer> newCaseMap = new HashMap<>(); 
    Map<String, List> domainConclusiondMap;
    String projectName;

    MySQLDatabase database = new MySQLDatabase();
    double domainSimThreshold;


    public Map<String, String> CBRapplication(String newCaseFile) throws IOException{
        Domain domain = new Domain();
        Map<String, String> casebase = new HashMap<>();
        Map<String, String> similarCasesWithDomain = new HashMap<>();
        List<String> relevantCases = new ArrayList<>();
        List<String> similarCases = new ArrayList<>();
        MySQLDatabase DB = new MySQLDatabase();
        
        System.out.println("Building Domains...");
        domain = buildDomain();
        
        domainConclusiondMap = domain.getCoclusionMap();

        System.out.println("Processing new case...");
        processNewCase(newCaseFile);
        
        relevantCases = getRelevantDomain(newCaseFile, domain);

        System.out.println("The relevant domain model cases are:");
        for (int i = 0; i < relevantCases.size(); i++) {
            System.out.println(relevantCases.get(i));
        }
        
        buildModelCasesMaps(relevantCases);
        
        similarCases = getSimilarCases(domainSimThreshold);
        
        casebase = DB.RetrieveCaseBase(projectName);
        
        for(String Case: similarCases){
            similarCasesWithDomain.put(Case, casebase.get(Case));
        }
        
        System.out.println("The similar cases are: ");
        for(String similarCase: similarCases){
            System.out.println(similarCase);
        }
        return similarCasesWithDomain;
    }
    
    public List<String> BuildCaseBase(Domain domain, Cases cases) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        List<String> AllClassifiedCases = new ArrayList<>();
        boolean nonDomain = false;
        MySQLDatabase DB = new MySQLDatabase();
        if(DB.isDatabaseCreated(projectName)){
            
            System.out.println("processing domains");

            
            if(domain.getSize()==0){
                nonDomain = true;
            }else{
                domain.processDomains_new();
                System.out.println("inserting domain to database...");
                domain.insertDB(projectName);
            }


            System.out.println("processing model cases");

            cases.processModelCases();
            cases.insertDB(projectName);

            System.out.println("Classifying model cases");

            for (int i = 0; i < cases.getCasesSize(); i++) {

                if(nonDomain){
                    AllClassifiedCases.add("none");
                }else{
                    String ClassifiedDomains = domainClassifier(cases.getCasesFilePathList(i), domain);

                    if (ClassifiedDomains == null) {
                        AllClassifiedCases.add("none");
                    } else {
                        AllClassifiedCases.add(ClassifiedDomains);
                    }
                }
            }

            DB.CreateDatabase(projectName, domain.getDomainFiles());
            DB.CreateCasebase(projectName, cases.getCasesList(), AllClassifiedCases);
        }else{
            JOptionPane.showConfirmDialog(null, "Building process stopped", "Database Error", JOptionPane.DEFAULT_OPTION);
        }
        return AllClassifiedCases;
    }
    
    
    public String domainClassifier(String CaseFilePath, Domain domains) throws IOException {
        String similarClassifiedDomains = null;
        Map<String, List> domainProfiles = domains.getKeywordMap();

        String strDoc = convertTextFileToString(CaseFilePath);


        for (int i = 0; i < domains.getSize(); i++) {
            String domainFile = domains.getDomainFileName(i);
            List<String> keyList = domainProfiles.get(domainFile);
            for (String w : keyList) {
                if(Pattern.compile(Pattern.quote(w), Pattern.CASE_INSENSITIVE).matcher(strDoc).find())
                    similarClassifiedDomains = domains.getDomainFileName(i);
            }

        }
        return similarClassifiedDomains;
    }
   
    public List<String> getRelevantDomain(String newCaseFile, Domain domains) throws IOException {
        String allClassifiedDomains = "";
        if(domains.getSize()!=0)
            allClassifiedDomains = domainClassifier(newCaseFile, domains);
        
        if(!allClassifiedDomains.isEmpty()){
            List<String> similarDomainModelCases = new ArrayList<>();
            similarDomainModelCases = database.RetrieveDomainModelCases(projectName, allClassifiedDomains);
            if(!similarDomainModelCases.isEmpty()){
                relevantDomainModelCases.addAll(similarDomainModelCases);
            }
        }else{
            List<String> similarDomainModelCases = new ArrayList<>();
            similarDomainModelCases = database.RetrieveDomainModelCases(projectName, "all");
            if (!similarDomainModelCases.isEmpty()) {
                relevantDomainModelCases.addAll(similarDomainModelCases);
            }
        }
        return relevantDomainModelCases; 
    }
    
    public int numOfDocsWithTerm(String term) {
        int termCount = 0;
        for (int i = 0; i < relevantDomainModelCases.size(); i++) {
            Map modelCaseMap = modelCasesMaps.get(i);
            if (modelCaseMap.keySet().contains(term)) {
                termCount += 1;
            }
        }
        return termCount; 
    }

    public List<Double> calculateTFIDF(List<String> terms, Map<String, Integer> map) {
        int totalNumOfDocs = relevantDomainModelCases.size();
        double tf;
        double idf;
        double tfidf = 0.0;
        List<Double> tfidf_list = new ArrayList<>();
        for (int i = 0; i < terms.size(); i++) {
            String currentTerm = terms.get(i);
            if (map.containsKey(currentTerm)) {
                int numOfOccurenceOfTerm = map.get(currentTerm);
                tf = (double) numOfOccurenceOfTerm;
                if(numOfDocsWithTerm(currentTerm) != 0){
                    int value = totalNumOfDocs - numOfDocsWithTerm(currentTerm); 
                    if(value == 0){
                        tfidf = 0; 
                    }else{
                        idf = Math.log10((double)value/(double) numOfDocsWithTerm(currentTerm));
                        tfidf = tf * idf;
                    }
                }else{
                    tfidf = 1.0;
                }
            }else{
                tfidf = 0.0; 
            }
            tfidf_list.add(tfidf);
        }
        return tfidf_list; 
    }
    
    public double Norm(List<Double> tfidf) {
        double sum = 0;
        for (int i = 0; i < tfidf.size(); i++) {
            sum += tfidf.get(i) * tfidf.get(i);
        }
        return Math.sqrt(sum);
    }
    
    public List<String> getSimilarCases(double minSim) throws IOException {
        List<String> terms = getTerms(newCaseMap);
        List<Double> tfidf_newCase = calculateTFIDF(terms, newCaseMap);
        Map<Double, String> similarCaseMap = new HashMap<>();

        List<String> similarCases = new ArrayList<>();
        List<Double> storeSim = new ArrayList<>();

        for (int i = 0; i < relevantDomainModelCases.size(); i++) {
            double similarity = 0.0;
            double innerProduct = 0.0;
            
            Map modelCaseMap = modelCasesMaps.get(i);
            List<Double> tfidf_modelCase = calculateTFIDF(terms, modelCaseMap);
            for (int k = 0; k < tfidf_newCase.size(); k++) { 
                innerProduct += tfidf_newCase.get(k) * tfidf_modelCase.get(k);
            }
            similarity = innerProduct / (Norm(tfidf_newCase) * Norm(tfidf_modelCase));
            similarCaseMap.put(similarity, relevantDomainModelCases.get(i));

            System.out.println("Similarity with " + relevantDomainModelCases.get(i) + " = " + similarity);
            
            if (similarity >= minSim)
                storeSim.add(similarity);
        }
        
        
        Collections.sort(storeSim);
        for(int i= 0; i < storeSim.size(); i++){
            if(storeSim.get(i) >= minSim)
                similarCases.add(similarCaseMap.get(storeSim.get(i)));
        }
        return similarCases;
    }
    
    
    public Domain buildDomain() throws IOException{
        Domain domain = new Domain();
        List<String> domainNames = new ArrayList<>();
        List<String> domainFilePath = new ArrayList<>();
        List<String> keywordList = new ArrayList<>();
        List<String> conKeywordList = new ArrayList<>();
        Map<String, List> keywordMap = new HashMap<>();
        Map<String, List> conclusionMap = new HashMap<>();

        domainNames = database.RetrieveNames(projectName, "domain");
        for(String dFile: domainNames){
            domainFilePath.add(database.getFilePath(projectName, dFile));
            keywordList = database.getKeywordList(projectName, dFile);
            conKeywordList = database.getKeywordList(projectName, dFile + "_conclusion");
            keywordMap.put(dFile, keywordList);
            conclusionMap.put(dFile, conKeywordList);
        }
        
        domain.setDomains(domainNames, domainFilePath, keywordMap, conclusionMap);
        return domain;
    }
    
    public void buildModelCasesMaps(List<String> relevantCases) {
        for(String casename: relevantCases){
            modelCasesMaps.add(database.RetrieveCase(projectName, casename));
        }
    }
    
    public void setProjectName(String name){
        projectName = name;
    }
    
    public void setDomainSimThreshold(double threshold){
        domainSimThreshold = threshold;
    }

    public List<String> getTerms(Map<String, Integer> caseMap) {
        List<String> terms = new ArrayList<>();
        for (String w : caseMap.keySet()) {
            terms.add(w);
        }
        return terms;
    }
    
    public String[] getTerms(String value) throws IOException {
        Directory index = new RAMDirectory();
        Version matchVersion = Version.LUCENE_35; 
        KStemmer stemmer = new KStemmer(matchVersion);
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_35, stemmer);
        try (IndexWriter writer = new IndexWriter(index, config)) {
            Document doc = new Document();
            Field field = new Field("title",value, Field.Store.NO, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS);
            doc.add(field);
            writer.addDocument(doc); 
            writer.commit();
        }

        IndexReader reader = IndexReader.open(index);

        TermFreqVector termFreqVector = reader.getTermFreqVector(0, "title");
        return termFreqVector.getTerms();
    }
    
    
    public int[] getTermsFreq(String value) throws IOException {
        Directory index = new RAMDirectory();
        Version matchVersion = Version.LUCENE_35;
        KStemmer stemmer = new KStemmer(matchVersion);
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_35, stemmer);
        try (IndexWriter writer = new IndexWriter(index, config)) {
            Document doc = new Document();
            Field field = new Field("title", value, Field.Store.NO, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS);
            doc.add(field);
            writer.addDocument(doc);
            writer.commit();
        }
        IndexReader reader = IndexReader.open(index);
        TermFreqVector termFreqVector = reader.getTermFreqVector(0, "title");
        return termFreqVector.getTermFrequencies();
    }
    
    
    public List<Map> processDomains(List<String> domains) throws IOException {
        List<String> domString = new ArrayList<>();
        for (int i = 0; i < domains.size(); i++) {
            domString.add(convertTextFileToString(domains.get(i)));
        }
        for (int i = 0; i < domString.size(); i++) {
            String[] Terms = getTerms(domString.get(i));
            int[] freq = getTermsFreq(domString.get(i));
            Map<String, Integer> domainMap = new HashMap<>();
            for (int j = 0; j < Terms.length; j++) {
                if (Terms[j].contains("'")) {
                    Terms[j] = Terms[j].replaceAll("'", " ");
                }
                domainMap.put(Terms[j], freq[j]); 
            }
        domainMaps.add(domainMap); 
        }
        return domainMaps;
    }
    
    
    public List<Map> processModelCases(List<String> modelCases) throws IOException {
        
        List<String> modelCasesString = new ArrayList<>();
        for (int i = 0; i < modelCases.size(); i++){
            modelCasesString.add(convertTextFileToString(modelCases.get(i)));
        }
        
        for (int i = 0; i < modelCasesString.size(); i++) {
            String[] Terms = getTerms(modelCasesString.get(i));
            int[] freq = getTermsFreq(modelCasesString.get(i));
            Map<String, Integer> modelCasesMap = new HashMap<>();
            for (int k = 0; k < Terms.length; k++){
                if (Terms[k].contains("'")) {
                    Terms[k] = Terms[k].replaceAll("'", " ");
                }
                modelCasesMap.put(Terms[k], freq[k]); 
            }
            modelCasesMaps.add(modelCasesMap); 
        }
        return modelCasesMaps;
    }
    
    
    public Map processNewCase(String newCaseFile) throws IOException {
        Map<String, Integer> caseMap = new HashMap<>();
        String newCaseString = convertTextFileToString(newCaseFile);
        String[] topTerms = getTerms(newCaseString);
        int[] freq = getTermsFreq(newCaseString);
        int i;
        for (i = 0; i < topTerms.length; i++) {
            if (topTerms[i].contains("'")) {
                topTerms[i] = topTerms[i].replaceAll("'", " ");
            }
            caseMap.put(topTerms[i], freq[i]); 
        }
        newCaseMap = caseMap;
        return newCaseMap;
    }
    

    public String convertTextFileToString(String docPath) throws IOException {
        StringBuilder everything = new StringBuilder();
        String line;

        try (BufferedReader br = new BufferedReader(new FileReader(docPath))) {
            while ((line = br.readLine()) != null) { 
                if (line.isEmpty()) {
                    everything.append(" ");
                    continue;
                }
                everything.append(line);
            }
        }
        return everything.toString();
    }
    
    public Map getConclusionMap(){
        return domainConclusiondMap;
    }
}
