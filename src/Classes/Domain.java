/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import cbr.CBR;
import cbr.MySQLDatabase;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author henry
 */
public class Domain {
    
    List<String> domainFileName = new ArrayList<>();
    List<String> domainFilePath = new ArrayList<>();
    List<Map> domainDocMaps = new ArrayList<>();
    Map<String, List> domainKeywordMap = new HashMap<>();
    Map<String, List> domainConclusiondMap = new HashMap<>();
    
    public void setDomains(List<String> FileName, List<String> FilePath, Map<String, List> keywordMap, Map<String, List> conclusionMap){
        domainFileName = FileName;
        domainFilePath = FilePath;
        domainKeywordMap = keywordMap;
        domainConclusiondMap = conclusionMap;
    }
    
    
    public void setInitialDomains(List<String> FileName, List<String> FilePath) {
        domainFileName = FileName;
        domainFilePath = FilePath;
    }
    
    public void addDomain(String FileName, String domainFullName){
        domainFileName.add(FileName);
    }
    
    public List<Map> getDomainMaps(){
        return domainDocMaps;
    }
    
    public int getSize(){
        return domainFileName.size();
    }
    
    
    public List<String> getDomainFiles(){
        return domainFileName;
    }
    
    public List<String> getDomainFilePath(){
        return domainFilePath;
    }
    
    public String getDomainFileName(int i){
        return domainFileName.get(i);
    }
    
    public Map getKeywordMap(){
        return domainKeywordMap;
    }
    
    public Map getCoclusionMap() {
        return domainConclusiondMap;
    }
    
    public void processDomainDoc() throws IOException{
        
        CBR domainProcessor = new CBR();
        domainDocMaps= domainProcessor.processDomains(domainFilePath);
        
    }
    
    public void processDomains_new() throws IOException {

        for (int i = 0; i < domainFilePath.size(); i++) {
            List<String> conclusionkeywords = new ArrayList<>();
            List<String> domainKeyword = new ArrayList<>();
            BufferedReader br = null;
            br = new BufferedReader(new FileReader(domainFilePath.get(i)));
            boolean read = false;

            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                if (sCurrentLine.equalsIgnoreCase("Keywords:")) {
                    read = true;
                    continue;
                }
                if (sCurrentLine.equalsIgnoreCase("Conclusion Keywords:")) {
                    read = false;
                    continue;
                }
                if (read && !sCurrentLine.isEmpty()) {
                    domainKeyword.add(sCurrentLine);
                } else if(!read && !sCurrentLine.isEmpty()) {
                    conclusionkeywords.add(sCurrentLine);
                }
            }
            domainKeywordMap.put(domainFileName.get(i), domainKeyword);
            domainConclusiondMap.put(domainFileName.get(i), conclusionkeywords);
        }
    }
    
    public void insertDB(String DBName) throws FileNotFoundException {
        MySQLDatabase database = new MySQLDatabase();

        for (int i = 0; i < domainFileName.size(); i++) {
            List<String> domainKeyword = domainKeywordMap.get(domainFileName.get(i));
            List<String> domainConclusion = domainConclusiondMap.get(domainFileName.get(i));
            database.insertDomain(DBName, domainFileName.get(i), domainKeyword, domainConclusion);
            database.saveCaseFile(DBName, domainFileName.get(i), domainFilePath.get(i));
        }
    }
}
