/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import cbr.CBR;
import cbr.MySQLDatabase;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author henry
 */
public class Cases {
    
    List<String> Cases = new ArrayList<>();
    List<String> CasesFilePath = new ArrayList<>();
    List<Map> ModelCasesMaps = new ArrayList<>();
    Map<String, Integer> newCase = new HashMap<>(); 
    
    public void setCases(List<String> casesFile, List<String> caseFilePath){
        Cases = casesFile;
        CasesFilePath = caseFilePath;
    }
    
    public void addCase(String caseFile){
        Cases.add(caseFile);
    }
    
    public Map getCaseMap(int i){
        return ModelCasesMaps.get(i);
    }
    
    public String getCaseName(int i){
        return Cases.get(i);
    }
    
    public List<String> getCasesList(){
        return Cases;
    }
    
    public String getCasesFilePathList(int i) {
        return CasesFilePath.get(i);
    }
    
    public int getCasesSize(){
        return Cases.size();
    }
    
    public void processNewCase() throws IOException{
        CBR CaseProcessor = new CBR();
        newCase = CaseProcessor.processNewCase(Cases.get(0));
    }
    
    public void processModelCases() throws IOException{
        CBR CaseProcessor = new CBR();
        ModelCasesMaps= CaseProcessor.processModelCases(CasesFilePath);
    }
    
    public void insertDB(String DBName) throws FileNotFoundException{
        MySQLDatabase database = new MySQLDatabase();
        
        database.CreateCaseList(DBName, Cases);
        
        for(int i=0; i<Cases.size(); i++)
        {
            Map<String, Integer> modelCasesMap = ModelCasesMaps.get(i);
            System.out.println("Inserting case " + Cases.get(i) + "...");
            database.CreateDatabase(DBName, Cases.get(i), modelCasesMap);
            System.out.println("Finish insert case " + Cases.get(i));
            database.saveCaseFile(DBName, Cases.get(i), CasesFilePath.get(i));
        }
    }
}
