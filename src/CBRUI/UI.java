/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CBRUI;

import Classes.Cases;
import Classes.Domain;
import cbr.CBR;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonModel;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author henry
 */
public class UI extends javax.swing.JDialog {

    /**
     * Creates new form UI
     */
    List<String> domainfilePath = new ArrayList<>();
    List<String> domainfileName = new ArrayList<>();
    List<String> casefilePath = new ArrayList<>();
    List<String> casefileName = new ArrayList<>();
    DefaultListModel domainFilelistModel = new DefaultListModel();
    DefaultListModel domainTitlelistModel = new DefaultListModel();
    DefaultListModel caseListModel = new DefaultListModel();
    Thread task;

    public UI(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        DevButtonGroup = new javax.swing.ButtonGroup();
        AppButtonGroup = new javax.swing.ButtonGroup();
        buttonGroup_classification = new javax.swing.ButtonGroup();
        jFrame1 = new javax.swing.JFrame();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        StatusTextBox = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        DomainFileList = new javax.swing.JList();
        OpenDomainFileButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        CaseFileList = new javax.swing.JList();
        OpenCaseFileButton = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        ProjectNameText = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        BuildCBRButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jFrame1Layout = new org.jdesktop.layout.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jFrame1Layout.createSequentialGroup()
                .add(137, 137, 137)
                .add(jButton1)
                .addContainerGap(166, Short.MAX_VALUE))
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jFrame1Layout.createSequentialGroup()
                .addContainerGap(183, Short.MAX_VALUE)
                .add(jButton1)
                .add(88, 88, 88))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("CBR Development");
        setBackground(new java.awt.Color(154, 154, 154));
        setLocation(new java.awt.Point(100, 100));

        StatusTextBox.setColumns(20);
        StatusTextBox.setRows(5);
        jScrollPane1.setViewportView(StatusTextBox);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("CBR development"));

        DomainFileList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "No file" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        DomainFileList.setToolTipText("");
        jScrollPane3.setViewportView(DomainFileList);

        OpenDomainFileButton.setText("+");
        OpenDomainFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpenDomainFileButtonActionPerformed(evt);
            }
        });

        CaseFileList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "No file" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        CaseFileList.setToolTipText("");
        jScrollPane2.setViewportView(CaseFileList);

        OpenCaseFileButton.setText("+");
        OpenCaseFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpenCaseFileButtonActionPerformed(evt);
            }
        });

        jButton2.setText("-");

        jLabel1.setText("Domain Files: (Optional)");
        jLabel1.setToolTipText("");

        jLabel3.setText("Case Files:");

        jLabel8.setText("Name of CBR project:");

        jButton3.setText("-");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel1)
                            .add(jPanel2Layout.createSequentialGroup()
                                .add(OpenDomainFileButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(4, 4, 4)
                                .add(jButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 44, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(jPanel2Layout.createSequentialGroup()
                                .add(jLabel8)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(ProjectNameText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE))
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jScrollPane3)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jScrollPane2)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2Layout.createSequentialGroup()
                                .add(OpenCaseFileButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel3))
                        .add(0, 11, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel8)
                    .add(ProjectNameText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 69, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(OpenDomainFileButton)
                    .add(jButton2))
                .add(8, 8, 8)
                .add(jLabel3)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(OpenCaseFileButton)
                    .add(jButton3))
                .addContainerGap())
        );

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Model Case", "Classified Domain"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane6.setViewportView(jTable2);

        BuildCBRButton.setText("Build CBR application");
        BuildCBRButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuildCBRButtonActionPerformed(evt);
            }
        });

        jMenu1.setText("Exit");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Mode");

        jMenuItem1.setText("CBR development");
        jMenu2.add(jMenuItem1);

        jMenuItem2.setText("CBR application");
        jMenuItem2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuItem2MouseClicked(evt);
            }
        });
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(203, 203, 203)
                        .add(BuildCBRButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 155, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jScrollPane6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 364, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 364, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(14, 14, 14)
                .add(jScrollPane6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 244, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 106, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(BuildCBRButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void OpenDomainFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpenDomainFileButtonActionPerformed

        Boolean MultSelectEnable = true;
        
        final JFileChooser fc = new JFileChooser();
        fc.setMultiSelectionEnabled(MultSelectEnable);
        int returnVal = fc.showOpenDialog(UI.this);
        File[] files = fc.getSelectedFiles();
        
        for(File file: files){
            domainFilelistModel.addElement(file.getName());
            domainfilePath.add(file.getAbsolutePath());
            domainfileName.add(file.getName());
        }

        
        DomainFileList.setModel(domainFilelistModel);
        
        StatusTextBox.append("Load domain files. \n");
        
    }//GEN-LAST:event_OpenDomainFileButtonActionPerformed

    private void OpenCaseFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpenCaseFileButtonActionPerformed
        // TODO add your handling code here:
        Boolean MultSelectEnable = true;
        final JFileChooser fc = new JFileChooser();
        fc.setMultiSelectionEnabled(MultSelectEnable);
        int returnVal = fc.showOpenDialog(UI.this);
        File[] filelist = fc.getSelectedFiles();

        for (File SFile : filelist) {
            caseListModel.addElement(SFile.getName());
            casefilePath.add(SFile.getAbsolutePath());
            casefileName.add(SFile.getName());
        }
        CaseFileList.setModel(caseListModel);

        StatusTextBox.append("Load case files \n");
    }//GEN-LAST:event_OpenCaseFileButtonActionPerformed

    private void BuildCBRButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuildCBRButtonActionPerformed
        // TODO add your handling code here:
        CBR cbr = new CBR();
        Domain domain = new Domain();
        Cases cases = new Cases();
        String[] colname = {"Case", "Domain"};
        List<String> ClassifiedDomains = new ArrayList<>();
        List<String> CasesList = new ArrayList<>();
        DefaultTableModel ClassifiedCaseListModel = new DefaultTableModel(colname, 0);
        ButtonModel selected;
        
        cbr.setProjectName(ProjectNameText.getText());
        StatusTextBox.append("Set database name \n");
        

        StatusTextBox.append("Initialising domains... \n");
        domain.setInitialDomains(domainfileName, domainfilePath);
        StatusTextBox.append("Set cases... \n");
        cases.setCases(casefileName, casefilePath);
        CasesList = cases.getCasesList();
        try {
            StatusTextBox.append("Building casebase... \n");
            ClassifiedDomains = cbr.BuildCaseBase(domain, cases);
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        }
        StatusTextBox.append("Build casebase complete \n");

        for (int i=0; i < CasesList.size(); i++) {
            ClassifiedCaseListModel.addRow(new Object[]{CasesList.get(i), ClassifiedDomains.get(i)});
        }
        
        jTable2.setModel(ClassifiedCaseListModel);
    }//GEN-LAST:event_BuildCBRButtonActionPerformed

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
        System.exit(0);
    }//GEN-LAST:event_jMenu1MouseClicked

    private void jMenuItem2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem2MouseClicked

    }//GEN-LAST:event_jMenuItem2MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                CBRApp newApp = new CBRApp(new javax.swing.JFrame(), true);
                newApp.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                newApp.setVisible(true);
            }
        });
        this.dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                UI dialog = new UI(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup AppButtonGroup;
    private javax.swing.JButton BuildCBRButton;
    private javax.swing.JList CaseFileList;
    private javax.swing.ButtonGroup DevButtonGroup;
    private javax.swing.JList DomainFileList;
    private javax.swing.JButton OpenCaseFileButton;
    private javax.swing.JButton OpenDomainFileButton;
    private javax.swing.JTextField ProjectNameText;
    private javax.swing.JTextArea StatusTextBox;
    private javax.swing.ButtonGroup buttonGroup_classification;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable2;
    // End of variables declaration//GEN-END:variables
}
